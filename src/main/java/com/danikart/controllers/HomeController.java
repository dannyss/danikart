package com.danikart.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	@RequestMapping({"", "/", "/home"})
	public String getHomePage() {
		return "home";
	}

	@RequestMapping("/danikart/buyer/home")
	public String getBuyerHome() {
		return "buyerHome";
	}
	
	@RequestMapping("/danikart/seller/home")
	public String getSellerHome() {
		return "sellerHome";
	}
}
