package com.danikart.repositories;

import org.springframework.data.repository.CrudRepository;

import com.danikart.components.CustomerOrder;

public interface OrderRepository extends CrudRepository<CustomerOrder, Long>{

}
