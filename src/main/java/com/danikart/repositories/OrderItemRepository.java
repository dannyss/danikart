package com.danikart.repositories;

import org.springframework.data.repository.CrudRepository;

import com.danikart.components.OrderItem;

public interface OrderItemRepository extends CrudRepository<OrderItem, Integer> {

}
