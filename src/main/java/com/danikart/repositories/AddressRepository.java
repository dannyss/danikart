package com.danikart.repositories;

import org.springframework.data.repository.CrudRepository;

import com.danikart.components.Address;

public interface AddressRepository extends CrudRepository<Address, Long>{

}
