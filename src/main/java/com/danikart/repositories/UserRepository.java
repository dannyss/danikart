package com.danikart.repositories;

import org.springframework.data.repository.CrudRepository;

import com.danikart.components.User;

public interface UserRepository extends CrudRepository<User, Long>{

}
