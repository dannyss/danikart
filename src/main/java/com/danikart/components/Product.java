package com.danikart.components;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

import lombok.NoArgsConstructor;

@Component
@Entity
@NoArgsConstructor
public class Product {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	@ElementCollection
	private List<String> highlights;
	private String description;
	@OneToMany
	private List<Review> reviews;
	@ManyToMany
	private Set<Category> categories;
	BigDecimal price;
	
    @Lob
    private List<Byte[]> images;

	public Product(Long id, String name, List<String> highlights, String description, List<Review> reviews, Set<Category> categories,
			BigDecimal price, List<Byte[]> images) {
		this.id = id;
		this.name = name;
		this.highlights = highlights;
		this.description = description;
		this.reviews = reviews;
		this.categories = categories;
		this.price = price;
		this.images = images;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public List<Byte[]> getImages() {
		return images;
	}

	public void setImages(List<Byte[]> images) {
		this.images = images;
	}

	public List<String> getHighlights() {
		return highlights;
	}

	public void setHighlights(List<String> highlights) {
		this.highlights = highlights;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
