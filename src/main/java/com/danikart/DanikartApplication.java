package com.danikart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class DanikartApplication {

	public static void main(String[] args) {
		SpringApplication.run(DanikartApplication.class, args);
	}
}
