package com.danikart.enums;

public enum OrderStatus {

    PENDING_PAYMENT, CONFIRMED, SHIPPED, IN_TRANSIT, DELIVERED, CANCELLED
}