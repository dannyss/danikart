package com.danikart.enums;

public enum Availability {
	
	IN_STOCK, OUT_OF_STOCK

}
